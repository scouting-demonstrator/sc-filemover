#! /usr/bin/python

if __name__ == "__main__":
    from os import listdir
    from os.path import isfile, join
    import time
    import os

    while True:
        try:
            onlyfiles = [f for f in listdir('/fff/ramdisk/scdaq') if isfile(join('/fff/ramdisk/scdaq', f)) and f.endswith('.dat')]
            if len(onlyfiles)==0:
                print "waiting for new file..."
                time.sleep(30)
            print onlyfiles
            for f in onlyfiles:
                full_filename = join('/fff/ramdisk/scdaq', f)
                outfile = f+'.bz2'
		name_without_extension = f.split(".")[0]
		subsystem=name_without_extension.split("_")[1]
		run_number=name_without_extension.split("_")[2]

                dest_path = join('/store/lustre/l1scout/scdaq/',run_number)

                if not (os.path.exists(dest_path)):
                    os.mkdir(dest_path)
                
                dest_name = join(dest_path,outfile) 
                if os.path.exists(dest_name):
                    counter = 1
                    while os.path.exists(dest_name+'.'+str(counter)):
                        counter += 1
                    dest_name = dest_name+'.'+str(counter)
                command = "lbzip2 "+full_filename+" -c > "+dest_name
                print "compressing "+full_filename
                retval = os.system(command)
                if retval==0:
                    command = "rm "+full_filename
                    print "deleting "+full_filename
                    retval = os.system(command)
        except OSError as err:
            print err
            time.sleep(600)

