#!/bin/bash -e
BUILD_ARCH=x86_64
SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
RHEL_MAJOR=$(cat /etc/redhat-release | cut -d' ' -f4 | cut -d'.' -f1)

echo "Checking if necessary build packages are installed..."
if [ "$RHEL_MAJOR" = "7" ]; then
  if ! rpm -q rpm-build; then
    echo "\nPlease install missing packages."
    exit 1
  fi
else
  if ! rpm -q gcc redhat-rpm-config rpm-build zlib-devel; then
    echo ""
    echo "Please install missing packages."
    exit 1
  fi
fi
echo "..Build dependencies OK"
echo ""

cd $SCRIPTDIR/..
BASEDIR=$PWD
PARAMCACHE="paramcache"
NLINES=1
ASK="1"
RAWVER=$(git describe --tags --long || echo 'v0.0.0') # If git describe fails, we set a default version number.
VER=$(echo ${RAWVER} | sed 's/^v//' | awk '{split($0,a,"-"); print a[1]}')
REL=$(echo "${RAWVER}" | sed 's/^v//' | awk '{split($0,a,"-"); print a[2]}')

echo "Version $VER, release $REL"
PACKAGENAMEFILEMOVER="sc-filemover"

cd $SCRIPTDIR/..
BASEDIR=$PWD

# create a build area
echo "removing old build area"
rm -rf /tmp/$PACKAGENAMEFILEMOVER-build-tmp
echo "creating new build area"
mkdir /tmp/$PACKAGENAMEFILEMOVER-build-tmp
cd /tmp/$PACKAGENAMEFILEMOVER-build-tmp
TOPDIR=$PWD
echo "working in $PWD"


# we are done here, write the specs and make the rpm
cat >sc-filemover.spec <<EOF
Name: $PACKAGENAMEFILEMOVER$pkgsuffix
Version: $VER
Release: $REL
Summary: L1 Scouting DAQ filemover
License: gpl
Group: CMS/L1Scouting
Packager: scouter
Source: none
%define _tmppath $TOPDIR/sc-filemover-build
BuildRoot: %{_tmppath}
BuildArch: $BUILD_ARCH
AutoReqProv: no
Provides:/opt/sc-filemover

%description
scouting daq filemover

%prep
cp -R $BASEDIR/scripts SOURCES/

%install
cd \$RPM_BUILD_ROOT
echo "Creating directories"
mkdir -p opt/sc-filemover
mkdir -p etc/sc-filemover
mkdir -p etc/logrotate.d
mkdir -p usr/lib/systemd/system
mkdir -p etc/init.d
cp $BASEDIR/init.d/sc-filemover.service usr/lib/systemd/system/sc-filemover.service
cp -R $BASEDIR/scripts opt/sc-filemover

%preun
if [ \$1 == 0 ]; then
  /usr/bin/systemctl stop sc-filemover || true
  /usr/bin/systemctl disable sc-filemover || true
  /usr/bin/systemctl stop sc-filemover || true
  /usr/bin/systemctl disable sc-filemover || true
fi

%files
%defattr(-, root, root, -)
/opt/sc-filemover/
%attr( 644 ,root, root) /usr/lib/systemd/system/sc-filemover.service

EOF
mkdir -p RPMBUILDFILEMOVER/{RPMS/{noarch},SPECS,BUILD,SOURCES,SRPMS}
rpmbuild --define "_topdir $(pwd)/RPMBUILDFILEMOVER" -bb sc-filemover.spec

